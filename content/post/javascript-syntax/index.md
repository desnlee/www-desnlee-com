---
title: JavaScript的基本语法
description:
author: DesnLee
date: 2021-03-21 18:00:00
slug: javascript-syntax
image:
tags: [JavaScript,JS,前端,语法]
categories:
    - JavaScript
---
# 表达式与语句

表达式是一组代码的集合，它返回一个值。每一个合法的表达式都能计算成某个值，但从概念上讲，有两种类型的表达式：有副作用的（*比如赋值*）和单纯计算求值的。


## JavaScript 的表达式类型

* **算数：** 得出一个数字, 例如 3.14159
* **字符串：** 得出一个字符串, 例如, "Fred" 或 "234"
* **逻辑值：** 得出 `true` 或者 `false`
* **基本表达式：** JavaScript 中基本的关键字和一般表达式
* **左值表达式：** 分配给左值


例如：

```javascript
1 + 2           
//表达式的值是3

add(1, 2)       
//表达式的值为函数的返回值（只有函数才有返回值）

console.log     
//表达式的值为函数本身

console.log(3)  
//表达式的值是undefined
```


## JavaScript的语句

```javascript
var a = 1       
//就是一个语句
```


## 两者区别
* 表达式一般都有值，但语句可能有，也可能没有
* 语句一般会改变环境（声明、赋值）


## 注意

> * JS大小写敏感
> * 只要不是断句的空格，就没有实际意义。可以任意加回车、空格
> * 但是 return 后面不能加回车


# 标识符的规则
* 第一个字符，可以是 `unicode字母` `$` `_` `中文`
* 之后的字符可以是数字


# if语句

## 语法

```javascript
if (表达式) {
    语句1;
} else {
    语句2;
}
```

> `{ }`在语句只有一句的时候可以省略


## 注意
* 表达式里可以非常变态，如 `a = 1`
* 语句1 和 语句2 里可以嵌套 `if else`
* 注意缩进及省略 `{ }` 的情况


## switch语句

```javascript
switch (fruit) {
    case "banana":
        //...
        break;
    case "apple":
        //...
        break;
    default:
    //...
}

// break 不能省略，除非需要判断多个条件
```


## 问号冒号表达式（三元表达式）
```javascript
表达式1 ? 表达式2 : 表达式3

//如果表达式1成立，返回表达式2，否则返回表达式3
//if else 各只有一句的时候可以用问号冒号表达式简化
```

## &&短路逻辑 和 ||短路逻辑
```javascript
A && B && C && D

//取第一个为假的值，如果都为真则取最后一个值，并不会取 true / false
//也是用来代替 if else 语句

例: fn && fn()  
//如果 fn 存在，就调用 fn


A || B || C || D
//取第一个为真的值，如果都为假则取最后一个值，并不会取 true / false
//也是用来代替 if else 语句

例: A = A || B  
//如果 A 不存在，则 A = B，B 是 A 的保底值
```


# 循环

## while循环

```javascript
var a =1         //初始化
while (表达式) {  //判断
    语句          //循环体
    a = a +0.1   //增长
}

// 1.判断表达式的真假
// 2.表达式为真，执行语句，执行完再返回判断表达式的真假
// 3.表达式为假，跳出循环，执行花括号后面的语句
```

## for循环

```javascript
for(语句1;表达式2;语句3){
    循环体
}

// 1.先执行语句1
// 2.然后判断表达式2
// 3.如果为真，执行循环体，然后执行语句3
// 4.如果为假，跳出循环，执行花括号后面的语句
```


## break 和 continue

* `break`  退出当前所有循环
* `continue`  退出当前这一次循环


# label
```javascript
foo: {            //标识符
console.log(1)    //花括号内打印 1
break foo;        //跳出
console.log(2);   //花括号内打印 2
}
console.log(3)    //花括号外打印 3

// 结果只会打出 1 和 3 ，因为在打印 2 之前使用了 break 跳出
```

&nbsp;

> ***资料来源：饥人谷、WikiPedia***
