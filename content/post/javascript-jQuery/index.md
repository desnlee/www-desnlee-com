---
title: jQuery 常用操作手册
description:
author: DesnLee
date: 2021-04-29
slug: javascript-jQuery
image: index.jpg
tags: [jQuery,JavaScript,JS,前端]
categories:
    - JavaScript
---
# jQuery 简介
jQuery 是一套跨浏览器的 JavaScript 库，简化 HTML 与 JavaScript 之间的操作。

据统计，全世界排名前 100 万的网站，有 76.85% 在使用 jQuery ，远远超过其他库（截止至2021年2月）。 [**点击查看统计数据 ->**](https://trends.builtwith.com/javascript/jQuery)

在 jQuery 诞生前，人们是使用原生 JS 来对 DOM 等一系列做操作的，但 JS 提供的 DOM API 有点反人类。

因此，经过前端程序员们不断的努力和尝试，发明了现在这套经典的 JavaScript 函数库。

# jQuery 常用操作

关于 jQuery 的全部 API，请查看 [**jQuery 中文文档**](https://www.jquery123.com)，以下仅对常用的操作进行罗列。

### 选择网页元素
jQuery 的基本设计思想和主要用法，就是 <span style="color:#cc272d">**选择某个网页元素，然后对其进行某种操作**</span> 。这是它区别于其他 Javascript 库的根本特点。

使用 jQuery 的第一步，就是一个选择表达式，放进构造函数 `jQuery()`（简写为$），然后就可以对该元素进行操作了。

选择表达式可以是 CSS 选择器，也可以是 jQuery 特有的表达式
```javascript
$(document)  //选择整个文档

$('#id')  //通过元素的 id 选择元素

$('div.class')  // 通过 class 属性选择 div 元素

$('input[name=name]')  // 通过 name 属性选择 input 元素

$('a:first')  //选择网页中第一个 a 元素

$('tr:odd')  //选择 tr 的奇数（odd）行

$('#form :input')  // 选择表单中的 input 元素

$('div:visible')  //选择可见（visible）的 div 元素

$('div:gt(2)')  // 选择除了前三个余下的 div 元素，数字为下标

$('div:animated')  // 选择当前处于动画状态的 div 元素
```

jQuery 设计思想之二，就是提供各种过滤器对结果集进行筛选进而缩小范围。
```javascript
$('div').has('p')  // 选择包含 p 元素的 div 元素

$('div').not('.class')  //选择 class 不为 class 的 div 元素

$('div').filter('.class')  //选择 class 为 class 的 div 元素

$('div').first()  //选择第 1 个 div 元素

$('div').eq(5)  //选择第 6 个 div 元素，数字为下标

$('div').next('p')  //选择 div 元素后面的第一个 p 元素

$('div').parent()  //选择 div 元素的父元素

$('div').closest('form')  //选择离 div 最近的 form 父元素

$('div').children()  //选择 div 的子元素

$('div').siblings()  //选择 div 的兄弟元素
```

### 链式操作 
jQuery 设计思想之三，就是选中网页元素以后，可以对它进行一系列操作，所有操作可以连接在一起，以链条的形式写出来。

这是 jQuery 最令人称道、最方便的特点。它的原理在于每一步的 jQuery 操作，返回的都是一个 **jQuery 对象**，该对象包含了所有 jQuery 操作，所以不同操作可以连在一起。

比如：
```javascript
$('div').find('h3').eq(2).html('Hello')

//在所有的 div 元素中，找到第 3 个 h3 元素，将其内容改为 Hello
```

jQuery 还提供了 `.end()` 方法，使得结果集可以后退一步。该后退指的是选择元素的后退，并非指还原到上一步操作。
```javascript
$('div').find('h3').eq(2).html('Hello').end().eq(3).html(`World`)

//在所有的 div 元素中，找到第 3 个 h3 元素，将其内容改为 Hello，然后退回到全部 h3 元素的结果集，选择第 4 个 h3 元素，将其内容改为 World
```

### 取值和赋值
jQuery 设计思想之四，就是使用同一个函数，来完成取值 `getter` 和赋值 `setter`，即 **取值器** 与 **赋值器** 合一。到底是取值还是赋值，由函数的参数决定。
```javascript
$('h1').html(); //直接调用函数，表示取出 h1 的值

$('h1').html('Hello'); //给函数传入参数 Hello，表示对 h1 进行赋值
```

常见的取值和赋值函数如下：
```javascript
.html()  //取出或设置 html 内容
    
.text()  //取出或设置 text 内容
    
.attr()  //取出或设置属性的值
    
.width()  //取出或设置元素的宽度
    
.height()  //取出或设置元素的高度
    
.val()  //取出表单元素的值
```

**需要注意的是**
> 如果结果集包含多个元素，那么赋值的时候，将对其中 **所有** 的元素赋值
> 
> 取值的时候，则是只取出 **第一个元素** 的值
> 
> `.text()` 例外，它会得到匹配元素集合中每个元素的合并文本，包括他们的后代

### 移动
jQuery 设计思想之五，就是提供两组方法，来操作元素在网页中的位置移动。

一组方法是直接移动该元素，另一组方法是移动其他元素，使得目标元素达到我们想要的位置。

> *假定我们选中了一个 `div` 元素，需要把它移动到 `p` 元素后面。*

- 第一种方法是使用 `.insertAfter()`，把 `div` 元素移动 `p` 元素后面
```javascript
$('div').insertAfter($('p'))
```

- 第二种方法是使用 `.after()`，把 `p` 元素加到 `div` 元素前面
```javascript
$('p').after($('div'))
```

这两种方法有一个差别，那就是 **返回的元素** 不一样。

第一种方法返回 `div` 元素，第二种方法返回 `p` 元素。可以根据需要，选择到使用哪一种方法。

### 复制、删除和创建
除了元素的位置移动之外，jQuery 还提供其他几种操作元素的重要方法。

- 复制元素使用 `.clone()`
- 删除元素使用 `.remove()` 和 `.detach()` 。两者的区别在于，前者不保留被删除元素的事件，后者保留，有利于重新插入文档时使用。
- 清空元素内容（但是不删除该元素）使用 `.empty()`

创建新元素的方法非常简单，只要把新元素直接传入 jQuery 的构造函数
```javascript
$('<p>Hello World</p>')

$('ul').append('<li>list item</li>')
```

### 工具方法
jQuery 设计思想之六,除了对选中的元素进行操作以外，还提供一些与元素无关的工具方法（utility）。不必选中元素，就可以直接使用这些方法。

它是定义在 jQuery 构造函数上的方法，即 `jQuery.method()`，所以可以直接使用。

而那些操作元素的方法，是定义在 jQuery 构造函数的 prototype 对象上的方法，即 `jQuery.prototype.method()`，所以必须生成实例（即选中元素）后使用。

常用的工具方法有以下几种：
```javascript
$.trim()  //去除字符串两端的空格

$.each()  //遍历一个数组或对象

$.inArray()  //返回一个值在数组中的索引位置。如果该值不在数组中，则返回-1

$.grep()  //返回数组中符合某种标准的元素

$.extend()  //将多个对象，合并到第一个对象

$.makeArray()  //将对象转化为数组

$.type()  //判断对象的类别（函数对象、日期对象、数组对象、正则对象等等）

$.isArray()  //判断某个参数是否为数组

$.isEmptyObject()  //判断某个对象是否为空（不含有任何属性）

$.isFunction()  //判断某个参数是否为函数

$.isPlainObject()  //判断某个参数是否为用"{}"或"new Object"建立的对象

$.support()  //判断浏览器是否支持某个特性
```

### 事件操作
jQuery 设计思想之七，就是把事件直接绑定在网页元素之上。
```javascript
$('p').click(
    function () {
        alert('Hello World')
    }
)
```

[**查看 jQuery 全部事件操作 ->**](https://www.jquery123.com/category/events/)

&nbsp;

> ***资料来源：饥人谷、阮一峰的网络日志、jQuery API 中文文档***
