---
title: 初识 Vue.js
description:
author: DesnLee
date: 2021-12-16
slug: vue2-basic
image: index.png
tags: [前端,Vue.js,框架]
categories:
    - Framework
---

# Vue.js 是什么
**Vue** 是一套用于 **构建用户界面**  的渐进式框架。具体介绍可以查看 [Vue 2 官方文档](https://cn.vuejs.org/v2/guide/index.html)

# Vue.js 的两个版本
- 完整版：[**vue.js**](https://cdn.bootcdn.net/ajax/libs/vue/2.6.11/vue.js)
- 非完整版：[**vue.runtime.js**](https://cdn.bootcdn.net/ajax/libs/vue/2.6.11/vue.runtime.js)


### 两个版本主要的区别
1. **compiler**：完整版有，非完整版无；
2. **视图写法**：完整版可写 HTML 或写在 template 选项，非完整版需要写在 render 函数中用 h 来创建标签；
3. 完整版有 compiler，有占位符或其他比较复杂的 Vue 语句时，可以通过分析 DOM 节点来修改，但是代码体积会比非完整版的大30%。可从 HTML 或 template 直接得到视图，功能更多；
4. 非完整版没有 compiler，体积更小，功能更弱。可以通过 webpack 里的 vue-loader 来 render 翻译，如将 HTML 变成 h('div',this.n)；


# template 和 render
- template 类似 html，可使用模版语法在其中加入 js 部分的变量
```html
<template>
    <div id="app">
        {{ n }}
        <button @click="add">+1</button>
   </div>
</template>
```
- render 函数接收一个参数 h，用这个参数去创建实例
```javascript
render(h){
    return h('div', [ this.n, h('button', { onClick: this.add }), '+1'])
    }
```

# 使用 CodeSandbox 写 Vue.js
- 打开 [CodeSandbox 官方网站](https://codesandbox.io/)；
- 注册登陆后可进入个人 Dashboard；
- 点击网站右上角 'Create Sandbox' 并从中选择 'Vue —— by CodeSandbox' 选项；
- 等待几秒，CodeSandbox 会自动帮你配置好 Vue.js 开发环境
- 然后就可以愉快的开始写 Vue.js 了！

![CodeSandbox Vue 项目示例](codesandbox.png)
