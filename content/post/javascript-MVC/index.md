---
title: 浅析MVC
description:
author: DesnLee
date: 2021-09-23
slug: javascript-mvc
image: index.jpg
tags: [MVC,设计模式,JavaScript,JS,前端]
categories:
- JavaScript
---
# MVC 是什么

MVC 是一种 **架构设计模式**，它让显示模块和数据模块独立出来，便于开发和后期维护。

在过去，MVC 被大量用于构建桌面和服务器端应用程序，如今 Web 应用程序的开发已经越来越向传统应用软件开发靠拢，Web 和应用之间的界限也进一步模糊。传统编程语言中的设计模式也在慢慢地融入 Web 前端开发。

MVC 最初是在研究 Smalltalk-80（1979年）期间设计出来的。当时的图形界面少之又少，施乐公司正在研发友好的用户图形界面，以取代电脑屏幕上那些拒人于千里之外的命令行和 DOS 提示符。

那时计算机世界出现了一个创世者，将现实世界抽象出模型形成 Model，将人机交互从应用逻辑中分离形成 View。

# MVC 的三个对象

- **Model（模型）：** 用于封装与应用程序的业务逻辑相关的数据以及对数据的处理方法，可能会有一个或多个视图监听此模型。一旦模型的数据发生变化，模型将通知有关的视图。
- **View（视图）：** 是它在屏幕上的表示，描绘的是 Model 的当前状态。当模型的数据发生变化，视图相应地得到刷新自己的机会。
- **Controller（控制器）：** 定义用户界面对用户输入的响应方式，起到不同层面间的组织作用，用于控制应用程序的流程，它处理用户的行为和数据 Model 上的改变。

&nbsp;

**其中涉及两种设计模式：**

* View 和 Model 之间的观察者模式，View 观察 Model，事先在此 Model 上注册，以便 View 可以了解在数据 Model上发生的改变。
* View 和 Controller 之间的策略模式

&nbsp;

**代码示例：**

```javascript
const eventBus = $(window)    //事件总线，M-V-C 之间通信

// Model，数据相关的增删改查等方法
const m = {
  data: {},
  crate() {},
  delete() {},
  update() { eventBus.trigger() },
  get() {}
}

// View，视图相关的渲染等方法
const v = {
  el: null,
  html: `html代码`,
  init(container) {},
  render() {}
}

//Controller，其余的方法
const c = {
  init() {
    v.init()
    v.render()
    c.autoBindEvents()
    eventBus.on()
   },
  events: {事件列表},
  autoBindEvents() {}
}
```

# EventBus API


EventBus一般用到以下三个API（用jQuery构造的对象）：

* ```on (eventName, fn)```：用于监听事件，eventName - 事件名字， fn - 执行函数；
* ```trigger (eventName, data)```：用于触发事件，eventName - 事件名字，data - 其他参数；
* ```off (eventName, fn)```：用于事件的解绑，eventName - 事件名字，fn - 解绑的函数。

&nbsp;

**代码示例：**

```javascript
const fn = () => {}

eventBus.trigger("updated")  // 触发 updated 事件
eventBus.on("updated", fn)  // 监听 updated 事件，如果 updated 事件被触发，执行 fn()
eventBus.on("updated", fn)  // 事件 updated 解绑函数 fn，如果 updated 事件被触发，不再执行 fn()
```

# 表驱动编程

**表驱动法就是一种编程模式(scheme)** —— 从表里面查找信息而不使用逻辑语句(if 和case)。

对简单的情况而言，使用逻辑语句更为容易和直白。而稍微复杂一些的逻辑则会构造出复杂、嵌套的逻辑判断代码，不利于阅读和维护。

而在某些情况下可以将多种情况与结果构造成一张表一一对应，即可不经过逻辑判断而根据情况查表得到需要的结果。

构造表的方法有很多，如构造查询键值、索引访问、阶梯访问等方式。

# 我理解的模块化

> “模块”在新华字典中的解释：１、在通信、计算机、数据处理控制系统的电路中，可以组合和更换的硬件单元。２、大型软件系统中的一个具有独立功能的部分。

看得出来，模块的解释特点就是：**独立、可组合可更换**。

所以，我理解的模块化，就是对一段代码根据功能、类型等具有相似特征的代码片段进行整理，将其分门别类封装成 **独立** 的代码，之后暴露出接口进行 **组合** 操作，实现之前的功能。

**模块化的好处显而易见**

- 利于维护：根据代码的功能或特性可直接定位到该代码应该在的文件，降低寻找难度及代码间互相影响的风险，并且一个功能代码只在一个地方，无需修改多处；
- 独立性：每一个模块都是独立的代码，可单独提供对应功能，与其他模块可无依赖关系；
- 代码复用：由于模块间业务解耦，则一些业务功能模块可在多个场景进行调用，减少重复代码的维护成本；
- 可分工：由于模块代码的独立性，则可进行拆分后多个团队协作开发，互相之间不会影响，提升开发效率。
