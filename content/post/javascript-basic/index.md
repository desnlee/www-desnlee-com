---
title: JavaScript初探
description:
author: DesnLee
date: 2021-03-21 12:00:00
slug: javascript-basic
image: index.jpg
tags: [JavaScript,JS,前端]
categories:
    - JavaScript
---
# JavaScript是什么
**JavaScript**（通常缩写为 JS ）是一种高级的、解释型的编程语言。JavaScript 是一门基于原型、函数先行的语言，是一门多范式的语言，它支持面向对象程序设计，命令式编程，以及函数式编程。它提供语法来操控文本、数组、日期以及正则表达式等，不支持I/O，比如网络、存储和图形等，但这些都可以由它的宿主环境提供支持。它已经由 ECMA（欧洲电脑制造商协会）通过 ECMAScript 实现语言的标准化。它被世界上的绝大多数网站所使用，也被世界主流浏览器（Chrome、IE、Firefox、Safari、Opera）支持。


JavaScript 与 Java 在名字或语法上都有很多相似性，但这两门编程语言从设计之初就有很大的不同，JavaScript 的语言设计主要受到了 Self（*一种基于原型的编程语言*）和 Scheme（*一门函数式编程语言*）的影响。在语法结构上它又与C语言有很多相似（*例如if条件语句、switch语句、while循环、do-while循环等*）。


在客户端，JavaScript 在传统意义上被实现为一种解释语言，但在最近，它已经可以被即时编译（JIT）执行。随着最新的 HTML5 和 CSS3 语言标准的推行它还可用于游戏、桌面和移动应用程序的开发和在服务器端网络环境运行，如 Node.js。


# 为什么要发明JavaScript
1994年，网景公司（*Netscape*）发布了 Navigator 浏览器 0.9 版。这是历史上第一个比较成熟的网络浏览器，轰动一时。但是，这个版本的浏览器只能用来浏览，不具备与访问者互动的能力。比如，如果网页上有一栏"用户名"要求填写，浏览器就无法判断访问者是否真的填写了，只有让服务器端判断。如果没有填写，服务器端就返回错误，要求用户重新填写，这太浪费时间和服务器资源了。


网景公司当时有两个选择：
> * 一个是采用现有的语言，比如 Perl、Python、Tcl、Scheme 等等，允许它们直接嵌入网页
> * 另一个是发明一种全新的语言。


这两个选择各有利弊。第一个选择，有利于充分利用现有代码和程序员资源，推广起来比较容易；第二个选择，有利于开发出完全适用的语言，实现起来比较容易。 到底采用哪一个选择，网景公司内部争执不下，管理层一时难以下定决心。


就在这时，发生了另外一件大事：1995年 Sun 公司将 Oak 语言改名为 Java，正式向市场推出。 Sun 公司大肆宣传，许诺这种语言可以 "一次编写，到处运行"（Write Once, Run Anywhere），它看上去很可能成为未来的主宰。网景公司动了心，决定与Sun公司结成联盟。它不仅允许 Java 程序以 applet（小程序）的形式，直接在浏览器中运行；甚至还考虑直接将 Java 作为脚本语言嵌入网页，只是因为这样会使HTML网页过于复杂，后来才不得不放弃。


总之，当时的形势就是，网景公司的整个管理层，都是 Java 语言的信徒，Sun 公司完全介入网页脚本语言的决策。因此，Javascript 后来就是网景和 Sun 两家公司一起携手推向市场的，这种语言被命名为 `Java + script` 并不是偶然的。


# JavaScript的诞生
此时，34岁的系统程序员 **Brendan Eich** 登场了。Brendan Eich 的主要方向和兴趣是函数式编程，网景公司招聘他的目的，是研究将 Scheme 语言作为网页脚本语言的可能性。Brendan Eich 本人也是这样想的，以为进入新公司后，会主要与 Scheme 语言打交道。


仅仅一个月之后，1995年5月，网景公司做出决策，未来的网页脚本语言必须"看上去与 Java 足够相似"，但是比 Java 简单，使得非专业的网页作者也能很快上手。这个决策实际上将 Perl、Python、Tcl、Scheme 等非面向对象编程的语言都排除在外了。


Brendan Eich 被指定为这种 "简化版Java语言" 的设计师。 但是，他对 Java 一点兴趣也没有。为了应付公司安排的任务，他只用 10 天时间就把 JavaScript 设计出来了。他觉得，没必要设计得很复杂，这种语言只要能够完成一些简单操作就够了，比如判断用户有没有填写表单。

&nbsp;

总的来说，他的设计思路是这样的： 
> 1. 借鉴 C 语言的基本语法； 
> 2. 借鉴 Java 语言的数据类型和内存管理； 
> 3. 借鉴 Scheme 语言，将函数提升到"第一等公民"（first class）的地位； 
> 4. 借鉴 Self 语言，使用基于原型（prototype）的继承机制。


所以，JavaScript 语言实际上是两种语言风格的混合产物 ----（简化的）函数式编程+（简化的）面向对象编程。这是由 Brendan Eich（函数式编程）与网景公司（面向对象编程）共同决定的。


# JavaScript的标准化
1996年11月，网景正式向 ECMA（欧洲计算机制造商协会）提交语言标准。

1997年6月，ECMA 以 JavaScript 语言为基础制定了 ECMAScript 标准规范ECMA-262。JavaScript 成为了 ECMAScript 最著名的实现之一。

&nbsp;

> ***资料来源：饥人谷、阮一峰的网络日志、WikiPedia***
