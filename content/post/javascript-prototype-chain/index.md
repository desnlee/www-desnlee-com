---
title: 原型链与继承
description:
author: DesnLee
date: 2021-07-06
slug: javascript-prototype-chain
image: index.jpg
tags: [JavaScript,JS,原型,继承,前端]
categories:
- JavaScript
---
  ## 原型链
JavaScript 每个对象都有一个私有属性 `__proto__`  指向它的构造函数的原型对象 `prototype`。该原型对象也有一个 `__proto__`，层层向上直到一个对象的原型对象`__proto__`为 `null`，作为这个原型链中的最后一个环节。

当试图访问一个对象的属性时，它不仅仅在该对象上搜寻，会在该对象的原型链上依次层层向上搜索，直到找到一个名字匹配的属性或到达原型链的末尾。

### 基于原型链的继承
在 JavaScript 中，函数是允许拥有属性的。所有的函数会有一个特别的属性 `prototype` 。可以通过 `Test.prototype` 为原型添加属性。

然后我们可以通过 `new` 操作符来创建基于这个原型对象的 Test 实例。使用 `new` 操作符，只需在调用 Test 函数语句之前添加 new。这样，便可以获得这个函数的一个实例对象。一些属性就可以添加到该原型对象中。
```javascript
function Test(){}    //创建一个函数
Test.prototype.f1 = `f1`    //为该函数原型添加自定义属性
let test1 = new Test()    //基于 Test 函数创建函数实例
test1.f1  // "f1"
```

### 基于 class 的继承
ECMAScript6 引入了一套新的关键字用来实现 class，它仍然基于原型。
```javascript
class Person {
  constructor(name, age) {
    this.name = name;
    this.age = age;
  }
  get hello() {
    console.log(`Hello, my name is ${this.name}, I am ${this.age} years old.`)
  }
}
// 创建一个Person类，它具有姓名，年龄和打招呼属性

let person1 = new Person(`jam`,18)    //创建一个Person的实例
person1.hello  // Hello, my name is jam, I am 18 years old.
```
