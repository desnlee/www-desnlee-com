---
title: JavaScript的变量声明
description:
author: DesnLee
date: 2021-04-03 14:00:00
slug: javascript-let-const
image:
tags: [JavaScript,JS,变量,前端,语法]
categories:
    - JavaScript
---
# JavaScript的变量声明
```javascript
var a = 1

let a = 1

const a = 1
```

- `var` 是过时的，不好用的方式
- `let` 是新的，更合理的方式
- `const` 声明时必须赋值且不能再改

> 一般用 `let` 声明变量， `const` 声明常量

# let声明
- 遵循块作用域，即使用范围为 `{}` 内部
- 不能重复声明
- 可以赋值，也可以不赋值
- 必须先声明，才能使用，否则报错
- 全局声明的 `let` 变量，不会变成 `window` 的属性，而 `var` 会创建 `window` 的属性
- `for` 循环配合 `let` 有奇效

# const声明
- 跟 `let` 几乎一样
- 声明时必须赋值，并且不能再改
- `for` 循环不能使用 `const`， 因为 `const` 变量的值不能被修改

<p style="font-size:16px; color:#cc272d"><strong>变量声明 指定了值,指定了类型,但是都可变化</strong></p>

# name 和 \`name\` 的区别
- name 是变量，值可以是任何东西
- \`name\` 是字符串常量，不会改变，只能是 \`name\`

&nbsp;

> ***资料来源：饥人谷***
