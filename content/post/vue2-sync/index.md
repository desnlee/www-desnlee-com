---
title: 理解 Vue.js 中的 .sync
description:
author: DesnLee
date: 2021-12-24
slug: vue2-sync
image: index.png
tags: [前端,Vue.js,框架]
categories:
    - Framework
---

# .sync 是什么
> 在有些情况下，我们可能需要对一个 prop 进行“双向绑定”。不幸的是，真正的双向绑定会带来维护上的问题，因为子组件可以变更父组件，且在父组件和子组件两侧都没有明显的变更来源。
>
> —— Vue 官方文档

从官方文档不难看出，`.sync` 的双向绑定其实不是真正的双向绑定，他只是一个语法糖，会被扩展为一个自动更新父组件属性的 `v-on` 监听器。

# 实例
#### App.vue
```html
<template>
  我有 {{ value }} 块钱
  <Button :value.sync = "number" />
  <Button :value = "number" v-on:update:value = "number = $event" />
</template>

<script>
  data(){
    return {
      number: 1
    }
  }
</script>
```
#### Button.vue
```html
<template>
  <button @click="$emit('update:value', value - 100)">点击花 100 块<button/>
</template>

<script>
  props:['value']
</script>
```
以上两种 `Input` 组件写法等价。

**由于 Vue 不允许子组件修改父组件的 props，但是我们又需要进行这种操作，那怎么办呢？**

于是当子组件触发需要修改 props 的事件时，子组件会传递一个更新事件给父组件，并且可以携带参数，父组件由此事件来进行修改自身的 props。**`.sync` 实际上就是它的语法糖。**
