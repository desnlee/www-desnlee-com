---
title: JavaScript 的跨域
description:
author: DesnLee
date: 2021-07-14 08:00:00
slug: javascript-cross-origin
image: index.jpg
tags: [跨域,CORS,JSONP,JavaScript,JS,前端]
categories:
    - JavaScript
    - Network
---
# 什么是同源
如果两个 URL 的 protocol、port(如果有指定的话)和 host 都相同的话，则这两个 URL 是同源。

同源策略是一个重要的安全策略，它用于限制一个 origin 的文档或者它加载的脚本如何能与另一个源的资源进行交互。它能帮助阻隔恶意文档，减少可能被攻击的媒介。

例如： 如果 JS 运行在 源A 里，那么就只能获取 源A 的数据，不能获取 源B 的数据，即不允许跨域。

# 什么是跨域
同源策略控制不同源之间的交互，例如在使用 `XMLHttpRequest` 或 `<img>` 标签时则会受到同源策略的约束。

这些交互通常分为三类：
- 跨域写操作（Cross-origin writes）一般是被允许的。例如链接（links），重定向以及表单提交。特定少数的HTTP请求需要添加 preflight。
- 跨域资源嵌入（Cross-origin embedding）一般是被允许。
- 跨域读操作（Cross-origin reads）一般是不被允许的，但常可以通过内嵌资源来巧妙的进行读取访问。

**跨域就是在不同源之间进行数据交互。**

# JSONP 跨域
由于同源策略，一般来说不同源之间无法进行沟通，而 HTML 的 `<script>` 元素是一个例外。利用 `<script>` 元素的这个开放策略，网页可以得到从其他来源动态产生的 JSON 资料，而这种使用模式就是所谓的 JSONP。

举一个具体例子，test1.com 想要访问 test2.com 的数据，
1. 首先 test2.com 先将数据写入一个文件 test2.com/data.js;
2. test1.com 事先定义好一个函数 `window.callback` ;
3. test1.com 通过 `<script>` 引用 test2.com/data.js ;
4. 然后 data.js 执行 `window.callback` 函数，test1.com 就可以通过 `window.callback` 获取到 test2.com 的数据了

# CORS 跨域
跨源资源共享 (CORS) 是一种基于 HTTP-Header 的机制，该机制通过允许服务器标示除了它自己以外的其它 origin（域，协议和端口），这样浏览器可以访问加载这些资源。

跨源资源共享还通过一种机制来检查服务器是否会允许要发送的真实请求，该机制通过浏览器发起一个到服务器托管的跨源资源的"预检"请求。在预检中，浏览器发送的头中标示有 HTTP 方法和真实请求中会用到的头。

实际操作中我们在不同源的资源响应头中加入 `Access-Control-Allow-Origin:https://test1.com` 即可让 test1.com 访问到不同源下的资源了。
