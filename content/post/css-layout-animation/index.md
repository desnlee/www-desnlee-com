---
title: CSS布局及动画
description: 
author: DesnLee
date: 2021-03-18 14:00:00
slug: css-layout-animation
image: index.jpg
tags: [CSS,布局,动画,前端,Flex,Grid]
categories:
    - CSS
---
# CSS布局是什么
布局是把页面分成一块一块，按左中右、上中下等排列。
- **固定宽度布局：** 一般 960px、1000px、1024px 等宽度。
- **不固定宽度布局：** 主要靠文档流的原理布局。
- **响应式布局：** PC上固定宽度，移动端不固定宽度，也就是混合布局。


> **布局的两种思路**
> 
> 从大到小——先定下大局，然后完善每个部分的小布局。
> 
> 从小到大——先完成小布局，然后组合成大布局。


## CSS布局之——Float浮动布局
`float` 属性指定一个元素应沿其容器的左侧或右侧放置，允许文本和内联元素环绕它。该元素将从网页的正常流动(*文档流*)中移除。

当一个元素浮动之后，它会被移出正常的文档流，然后向左或者向右平移，一直平移直到碰到了所处的容器的边框，或者碰到另外一个浮动的元素。

最早是为了图文混排，兼容 IE 则需 `float` 布局，否则可直接用 `flex` 或者 `grid` 布局，其更加强大。


[具体使用方式参考 MDN 文档 ->](https://developer.mozilla.org/zh-CN/docs/Web/CSS/float)


## CSS布局之——Flex弹性布局
`flex` 属性设置了弹性项目如何增大或缩小以适应其弹性容器中可用的空间。 此方案为目前网页主流布局方式。


[具体使用方式参考 MDN 文档 ->](https://developer.mozilla.org/zh-CN/docs/Web/CSS/flex)


## CSS布局之——Grid网格布局
`grid` 属性定义了一个 CSS 网格。接着就可以使用属性给网格划分区域，然后就可以在这些网格中自由选择区域放置内容，称为网格布局。网格布局比 flex 弹性布局更加强大，但目前此方案浏览器兼容性比较差。


[具体使用方式参考 MDN 文档 ->](https://developer.mozilla.org/zh-CN/docs/Web/CSS/grid)


# 浏览器渲染原理
浏览器拿到一个网页的文件后，依次做出以下的处理，然后浏览器就可以显示出网页渲染后的样子：

> 1. 根据HTML构建HTML树（DOM）
> 2. 根据CSS构建CSS树（CSSOM）
> 3. 将两棵树合并成一棵渲染树（render tree）
> 4. Layout 布局（文档流、盒模型、计算大小和位置）
> 5. Paint 绘制（把边框颜色、文字颜色、阴影等画出来）
> 6. Composite 合成（根据层叠关系展示画面）


# CSS动画及性能优化
通过更新元素的CSS属性，使得元素有变化，就是一个CSS动画。三种更新方式为：

* 更新 Layout，整个页面布局 relayout，从第 4 步开始将页面重新渲染一次。
* 更新 Paint，将元素的 paint 属性更新，从第 5 步开始将页面重新渲染一次。
* 更新 Composite，仅仅只需重走第 6 步。


依据上面的渲染原理可得出，改变的属性在浏览器渲染步骤中越靠后，浏览器重新渲染所需的步骤资源就越少，性能就越好。
> 例如使用 `will-change` 或 `translate`


# CSS动画的实现方式
## transform（变形） & transition（过渡）
`transform` 属性允许你旋转，缩放，倾斜或平移给定元素。这是通过修改CSS视觉格式化模型的坐标空间来实现的。

`transition` 属性可以被指定为一个或多个 CSS 属性的过渡效果。


通过 `transform` 属性，设置一个触发条件，然后为元素分别设置触发前后的状态，最后给元素加上 `transition` 属性让浏览器自动补充中间帧来过渡，达到触发加载动画的效果。

[transform 使用方式参考 MDN 文档 ->](https://developer.mozilla.org/zh-CN/docs/Web/CSS/transform)

[transition 使用方式参考 MDN 文档 ->](https://developer.mozilla.org/zh-CN/docs/Web/CSS/transition)


***但是，并不是所有的属性都能过渡***



## animation
通过 `@keyframes` 语法添加关键帧，然后用 `animation` 把关键帧赋予元素，使该元素依照 `@keyframe` 定义的行为运动。

[transition 使用方式参考 MDN 文档 ->](https://developer.mozilla.org/zh-CN/docs/Web/CSS/animation)

&nbsp;

> ***资料来源：饥人谷、MDN***
