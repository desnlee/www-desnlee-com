---
title: HTML入门笔记
description:
author: DesnLee
date: 2021-03-14 12:00:00
slug: html-basic
image: index.jpg
tags: [HTML,前端]
categories:
    - HTML
---
# HTML是什么
**超文本标记语言**（*HyperText Markup Language，简称：HTML*），是一种用于创建网页的 **标准标记语言**。


# HTML的历史
HTML的首个公开描述出现于一个名为 `HTML Tags` 的文件中，由 **蒂姆·伯纳斯-李（Tim Berners-Lee）** 于 **1991年底** 提及。

它描述18个元素，包括HTML初始的、相对简单的设计。[点击查看 HTML Tags](https://web.archive.org/web/20100131184344/http:/www.w3.org/History/19921103-hypertext/hypertext/WWW/MarkUp/Tags.html)

**1993年中期** ，互联网工程任务组（IETF）发布首个HTML规范的提案是由 **伯纳斯-李** 与 **丹·康纳利** 撰写。


# HTML版本时间线
* 1995年11月24日，HTML 2.0作为IETF RFC 1866发布。
* 1997年1月14日，HTML 3.2作为W3C推荐标准发布。这是首个完全由W3C开发并标准化的版本，因IETF于1996年9月12日关闭了它的HTML工作组。
* 1997年12月18日，HTML 4.0作为W3C推荐标准发布。
* 1999年12月24日，HTML 4.01作为W3C推荐标准发布。
* 2000年5月，ISO HTML（基于HTML 4.01 严格版）作为ISO/IEC国际标准发布。
* 2014年10月28日，HTML 5作为W3C推荐标准发布。


# HTML入门
## HTML5的两个含义
1. 指目前最新版的 HTML 语言，含旧的标签和32个新的标签；
2. HTML5和他的朋友们（包括CSS3等）。

> *注：平时产品经理提及的H5页面，一般指代的是移动端页面而不是HTML5，不要被外行误导。*

## HTML文件的头部（起手式）
```html
<!DOCTYPE html>
<html lang="zh-CN">
<head>
  <meta charset="UTF-8">
  <meta name="viewport"
        content="width=device-width,initial-scale=1.0">
  <meta http-equiv="X-UA-Compatible" content="ie=edge">
  <title>Document</title>
</head>
<body>

</body>
</html>
```

## HTML的语法
```html
<!DOCTYPE html>
<!--声明文档类型为html格式-->

<tag attr=value>内容<tag/>
<!--有值的属性，如：id、class...-->

<tag attr>内容</tag>
<!--布尔属性（写则有不写则无），如：checked...-->

<tag attr=value />
<!--单标签的旧版写法-->

<tag attr=value >
<!--单标签的新版写法，如：img、link...-->
```
## HTML的基本知识
### 章节标签
* 标题 `h1～h6`
* 章节 `section`
* 文章 `article`
* 主要内容 `main`
* 旁支内容 `aside`
* 段落 `p`
* 头部 `header`
* 底部（脚部） `footer`
* 划分 `div`

### 内容标签（常用）
* 有序列表 `ol & li`
* 无序列表 `ul & li`
* 用来描述的列表 `dl & dt & dd`
* 需要连续空格/换行要用 `pre` 包裹内容
* 分割线 `hr`
* 换行 `br`
* 超链接 `a`
* 强调内容（语气上的强调） `em`
* 重点内容（本身就是重点） `strong`
* 代码内容 `code`
* 内联引用 `quote`
* 块级引用 `blockquote`

### 全局属性
**tag = value**
```html
<p class="xxx"></p>
<!--通过 .classname 选择，可同时有多个class-->

<p id="xxx"></p>
<!--通过 #id 选择，尽量不用，会与 window 的属性冲突-->

<p style="xxx:yyy"></p>
<!--写法和 CSS 相同，优先级比 CSS 高；JS 的优先级比 style 更高-->

<p tabindex=-1/0/1...></p>
<!--Tab 键切换可交互元素的顺序-->
<!-- -1 则不访问，0 为最后访问，其余的由小到大顺序选择-->

<img title="我是描述">
<!--元素 hover 时弹出气泡的内容，可用来显示被隐藏的元素的完整内容-->
```

**加了就有的属性**
```html
<p contenteditable></p>
<!--为该元素添加可以被编辑的属性,用户就可以直接在网页上编辑该内容-->

<img hidden>
<!--使该元素隐藏起来,使用CSS的 display: block 可以重新显示该内容-->
```
## HTML的默认样式

Chrome开发者工具中的 `user agent stylesheet` 就是 HTML 的默认样式。
一般都很丑，通过创建一个 CSS 文件将部分默认样式统一。

&nbsp;

> ***资料来源：饥人谷、WikiPedia***
