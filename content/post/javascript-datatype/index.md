---
title: JavaScript的数据类型
description:
author: DesnLee
date: 2021-04-03 12:00:00
slug: javascript-datatype
image:
tags: [JavaScript,JS,数据类型,前端]
categories:
    - JavaScript
---
# JavaScript的数据类型

<p style="font-size:16px; color:#cc272d"><strong>四基两空一对象：</strong></p>

* 数字 `number`
* 字符串 `string`
* 布尔 `bool`
* 符号 `symbol`
* 空 `undefined`
* 空 `null`
* 对象 `object`

> 注意：数组，函数，日期不是数据类型，都属于对象 `object`

# 数字（64位浮点数）

## JS里数字的写法
JS里数字可以用以下几种写法来写：

```javascript
1                         //整数写法
0.1                       //小数写法
1.23e4                    //科学计数法
0123 或 00123  或 0o123    //八进制写法，较少
0x3F  或  0X3F             //十六进制写法
0b11  或  0B11             //二进制写法
```

## JS数字中的特殊值
* 正 0  和 负 0 不一样，但都等于 0
* 无穷大 `Infinity` `+Infinity` `-Infinity`
* 无法表示的数字 `NaN`（Not a Number），但它是一个数字

## 64位浮点数
**浮点**（floating point，缩写为FP）是一种对于实数的近似值数值表现法，由一个有效数字（尾数）加上幂数来表示，通常是乘以某个基数的整数次指数得到。以这种表示法表示的数值，称为浮点数（floating-point number）。

> 例：123.456 可以表示为 1.23456e10^2

### 64浮点数的存储格式
* 符号占1位，指数占11位（-1023 ～ 1024）
* 有效数字占52位（开头的1省略）

![浮点数的存储格式](floatPointNumber.png)

# 字符串

## 写法
```javascript
""    //双引号
''    //单引号
``    //反引号

//字符串需要包含引号时用转义符 \
//反引号内可以直接换行，直接使用引号
// 用反引号比较万能，尽量使用反引号
```

## 属性
```javascript
string.length  //字符串长度
string[0]      //字符串的第一个字符，顺延+1，以此类推
```

# 数字与字符串的区别

## 区别点

**功能不同：**
* 数字是数字，字符串是字符串
* 数字能加减乘除，字符串不行
* 字符串能表示电话号码，数字不行

**储存形式不同：**
* 数字是用 64 位浮点数的形式存储的
* 字符串是用类似 `UTF-8` 形式存储的（UCS-2）

## 如何存
* **存数字：** 十进制转二进制即可
* **存字符串：**  用编码对应，存编号

# 布尔
**布尔**（Boolean）是计算机科学中的逻辑数据类型，以发明布尔代数的数学家 **乔治·布尔** 为名。
它是只有两种值的原始类型，通常是 **真** 和 **假**。
布尔数据类型主要与条件语句相关系，条件语句通过根据开发人员指定的条件式，更改程序控制流来允许评估语句的运算值为真或假(即条件成立或不成立)。

## 什么运算可以得到bool值
```javascript
!value      //否定运算
1 == 2      //相等运算
1 > 2       //比较运算
```

## 五个 `falsy` 值
<p style="font-size:16px; color:#cc272d"><strong>falsy 值就是相当于 false 但又不是 false 的值</strong></p>

`undefined`  `null`  `0`  `NaN`  `''`

# 两种空类型

`undefined` 和 `null`没有本质区别

- 如果一个变量声明了，但是没有赋值，那么默认值就是  `undefined` 而不是 `null`
- 如果一个函数，没有写 `return` ，那么默认 `return` 的是 `undefined` 而不是 `null`
- 习惯上把非对象的空值写为 `undefined`，把对象的空值写为 `null`

# symbol
symbol不常用，可以查看知乎的文章了解一下。

[「每日一题」JS 中的 Symbol 是什么？ ->](https://zhuanlan.zhihu.com/p/22652486)

# 数据类型的转换

## number ⇒ string
```javascript
String(n)

n + ``
```

## string ⇒ number
```javascript
Number(s)

s - 0

+s

parseInt('123')
```

## x ⇒ bool
```javascript
Boolean(x)

!!x
```

## x ⇒ string
```javascript
x.toString()

1..toString()
(1).toString()
//如果转换数字用两个 . 或者给数字加括号
```

&nbsp;

> ***资料来源：饥人谷、WikiPedia***
