---
title: "About"
date: 2021-03-31
layout: "post"
slug: "about"
image: page/about.jpg
menu:
    main:
        weight: -70
        pre: user
---
# Hello ~
我是李佳坤，一个学习前端的野生~~UI设计师~~。

<br>

# Me
**DesnLee - 李佳坤**

坐标 **杭州** ，前端萌新（学习中），在职UI设计师。

* 平时常在 Telegram 水群，研究脚本，网络；
* 看网文，《万族之劫》《轮回乐园》粉。

<br>

### 希望和你做朋友 ^_^ ~